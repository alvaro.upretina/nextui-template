import OpenAI from "openai";
import { Message, StreamingTextResponse } from "ai";

// Create an OpenAI API client (that's edge friendly!)
const openai = new OpenAI({
  apiKey: process.env.OPENAI_API_KEY,
});

// Set the runtime to edge for best performance
export const runtime = "edge";

export async function POST(req: Request) {
  const { prompt } = await req.json();

  // Ask OpenAI for a streaming completion given the prompt
  const assistant = await openai.beta.assistants.retrieve(
    "asst_GHtdn4YKCErkBiXfZximpBTZ"
  );

  let thread = await openai.beta.threads.create();

  openai.beta.threads.messages.create(thread.id, {
    content: prompt,
    role: "user"
  } as any);

  const run = await openai.beta.threads.runs.create(thread.id, {  assistant_id: assistant.id });

  let runCompleted = false;
    let messages : any;

    while (!runCompleted) {
        const runStatus = await openai.beta.threads.runs.retrieve(thread.id, run.id);
        if (runStatus.status === "completed") {
            runCompleted = true;
            messages = await openai.beta.threads.messages.list(thread.id);            
            console.log("completed");
          } else {
            await new Promise(r => setTimeout(r, 200));
          }

    }
    
    let response_text = messages.data.filter((message: Message) => message.role === "assistant");
    const last_response = response_text[0].content[0].text.value;

    // convert last_response to a ReadableStream
    const readableStream = new ReadableStream({
      start(controller) {
        const encoder = new TextEncoder();
        controller.enqueue(encoder.encode(last_response));
        controller.close();
      }
    });

    // Respond with the stream
    return new StreamingTextResponse(readableStream);

}
