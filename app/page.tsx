'use client'

import { Button } from '@nextui-org/button';
import { Card, CardBody } from '@nextui-org/card';
import { Textarea } from '@nextui-org/input';
import { Skeleton } from '@nextui-org/skeleton';
import { useCompletion } from 'ai/react';

export default function Home() {
	const { completion, input, handleInputChange, handleSubmit, isLoading } = useCompletion();

	return (
		<section className="">
			<form onSubmit={handleSubmit}>
        <input type="hidden" name="assistant_id" value="1234"/>
        <div className="flex flex-row gap-4">
          <div className="flex-1">
            <Textarea
              fullWidth
              minRows={10}
              maxRows={30}
              label="Input"
              placeholder="El paciente presenta..."
              value={input}
              onChange={handleInputChange}
            />
            <Button color="primary" isDisabled={input.length < 10} isLoading={isLoading} type="submit" className="mt-4">
              Generate
            </Button>
          </div>
          <div className="flex-1">
            <Card>
              <CardBody>
              {completion ? (
                    <div className="whitespace-pre-wrap">{completion}</div>
              ) : isLoading  ? (
                  <div className="space-y-3">
                    <Skeleton className="w-3/5 rounded-lg">
                      <div className="h-3 w-3/5 rounded-lg bg-default-200"></div>
                    </Skeleton>
                    <Skeleton className="w-4/5 rounded-lg">
                      <div className="h-3 w-4/5 rounded-lg bg-default-200"></div>
                    </Skeleton>
                    <Skeleton className="w-2/5 rounded-lg">  
                      <div className="h-3 w-2/5 rounded-lg bg-default-300"></div>
                    </Skeleton>
                  </div>
                ) : (
                      <p>Introduce el texto</p>
                )}              
              </CardBody>
              </Card>         
            
          </div>
        </div>
      </form>
		</section>
	);
}
